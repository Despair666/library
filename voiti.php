<?
    require "db.php";

    $data = $_POST;
    $errors = [];

    if (isset($data["sign_in"])) {
        if (trim(($data["login"])) == "") {
            $errors[] = "Не указан логин";
        }

        if (trim($data["password"]) == "") {
            $errors[] = "Не указан пароль";
        }

        if (empty($errors)) {
            $login = strtolower(trim($data["login"]));
            $password = $data["password"];
            $user = R::findOne("users", "login = ?", [$login]);
            if (isset($user)) {
                if (password_verify($password, $user->password)) {
                    $_SESSION["polzovatel"] = $user;
                    header("Location: /");
                } else {
                    $errors[] = "Неверно указан пароль";
                }
            } else {
                $errors[] = "Пользователь с таким логином не найден";
            }
        }
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Библиотека</title>
        <link rel="stylesheet" href="/css/index.css">
        <link rel="stylesheet" href="/css/voiti.css">
    </head>
    <body>
        <header>
            <ul>
                <li><a href="/">Главная</a></li>
                <?
                    if (isset($_SESSION["polzovatel"])) {
                        if ($_SESSION["polzovatel"]->role == "admin") {
                            echo "<li><a href=\"/polzovateli.php\">Пользователи</a></li>";
                        } else {
                            echo "<li><a href=\"/knigi.php\">Книги</a></li>";
                        }
                    }
                ?>
                <li>
                    <?
                        if (isset($_SESSION["polzovatel"])) {
                            echo "<a href=\"/vyiti.php\">Выйти</a>";
                        } else {
                            echo "<a href=\"/voiti.php\">Войти</a>";
                        }
                    ?>
                </li>
            </ul>
        </header>
        <main>
            <form action="/voiti.php" method="POST">
                <?
                    if (isset($data["sign_in"])) {
                        if (!empty($errors)) {
                            echo "<div>".$errors[0]."</div><hr>";
                        }
                    }
                ?>
                <table>
                    <tr>
                        <td>Логин</td>
                        <td><input type="text" name="login" value="<?= $data["login"] ?>" autofocus></td>
                    </tr>
                    <tr>
                        <td>Пароль</td>
                        <td><input type="password" name="password"></td>
                    </tr>
                </table>
                <button type="submit" name="sign_in">Войти</button>
            </form>
        </main>
    </body>
</html>
