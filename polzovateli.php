<?
    require "db.php";
    require "function.php";
    if (!isset($_SESSION["polzovatel"]) || $_SESSION["polzovatel"]->role != "admin") {
        header("Location: /");
    }
    $data = $_POST;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Библиотека</title>
        <link rel="stylesheet" href="/css/index.css">
        <link rel="stylesheet" href="/css/tablica.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="/js/polzovateli.js" defer></script>
    </head>
    <body>
        <header>
            <ul>
                <li><a href="/">Главная</a></li>
                <li><a href="/polzovateli.php">Пользователи</a></li>
                <li>
                    <?
                        if (isset($_SESSION["polzovatel"])) {
                            echo "<a href=\"/vyiti.php\">Выйти</a>";
                        } else {
                            echo "<a href=\"/voiti.php\">Войти</a>";
                        }
                    ?>
                </li>
            </ul>
        </header>
        <main>
            <h1>Пользователи</h1>
            <table>
                <tr>
                    <th>Логин</th>
                    <th>Роль</th>
                    <th>Удалить</th>
                </tr>
                <?
                    $users = R::findAll("users", "ORDER BY login ASC");
                    foreach ($users as $user) {
                        if ($_SESSION["polzovatel"]->login == $user["login"]) {
                            echo "<tr><td>".$user["login"]."</td><td>".role_full($user["role"])."</td><td></td></tr>";
                        } else {
                            echo "<tr><td>".$user["login"]."</td><td>".role_full($user["role"])."</td><td><button class=\"remove-user\" data-login=\"".$user["login"]."\">Удалить</button></td></tr>";
                        }
                    }
                ?>
            </table>
            <br>
            <h3>Логин: <input type="text" id="login"> Пароль: <input type="password" id="password"> Роль: <select id="role"><option value="client"><?= role_full("client") ?></option><option value="librarian"><?= role_full("librarian") ?></option><option value="admin"><?= role_full("admin") ?></option></select><button id="add-user">Добавить</button></h3>
        </main>
    </body>
</html>
