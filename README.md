## Библиотека

---

**PHP 5.6**

**PostgreSQL 9.6**

---

Файл конфигурации - **config.php**.

Данные для подключения к базе данных устанавливаются в файле конфигурации.

Логин и пароль администратора устанавливаются в файле конфигурации.

Для добавления 2 тестовых пользователей (пользователь и библиотекарь) и 3 тестовых книг необходимо в файле конфигурации установить **USERS_N_BOOKS** как **true**.

Таблицы **users** и **books** создаются автоматически.

---

**Данные администратора по умолчанию:**

Логин: *admin*; Пароль: *admin123*


**Данные тестовых пользователей:**

1. Логин: *librarian*; Пароль: *librarian123*; Роль: библиотекарь

2. Логин: *client*; Пароль: *client123*; Роль: клиент
