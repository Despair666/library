<?
require "../db.php";

if ($_SESSION["polzovatel"]->role == "client") {

    $data = $_POST;
    $errors = [];
    $success = "false";

    $name = $data["name"];
    if (isset($name)) {
        $book = R::findOne("books", "name = ?", [$name]);
        if (isset($book)) {
            $book->booked = $_SESSION["polzovatel"]->login;
            $book->book_date = strtotime("+2 days");
            R::store($book);
        } else {
            $errors[] = "Книга с таким названием не существует";
        }
        if (empty($errors)) {
            $success = "true";
        }
    } else {
        $errors[] = "Недостаточно данных";
    }

    echo "{\"success\":".$success.",\"error\":\"".$errors[0]."\"}";

}
