$("#add-book").on("click", () => {
    let name = $("#name").val().trim()
    let author = $("#author").val().trim()
    let genre = $("#genre").val().trim().toLowerCase()
    let publisher = $("#publisher").val().trim()
    let errors = []
    if (name == "") {
        errors.push("Не указано название")
    } else if (author == "") {
        errors.push("Не указан автор")
    } else if (genre == "") {
        errors.push("Не указан жанр")
    } else if (publisher == "") {
        errors.push("Не указан издатель")
    }
    if (errors.length > 0) {
        alert(errors[0])
    } else {
        $.post("/api/dobavitKnigu.php", { name, author, genre, publisher }, data => {
            data = JSON.parse(data)
            if (data.success) {
                alert("Книга \"" + name + "\" успешно добавлена")
                location.reload()
            } else {
                alert(data.error)
            }
        })
    }
})

$(".remove-book").on("click", function() {
    let name = $(this).data("name")
    if (confirm("Удалить книгу \"" + name + "\"?")) {
        $.post("/api/udalitKnigu.php", { name }, data => {
            data = JSON.parse(data)
            if (data.success) {
                alert("Книга \"" + name + "\" успешно удалена")
                location.reload()
            } else {
                alert(data.error)
            }
        })
    }
})

$(".book-book").on("click", function() {
    let name = $(this).data("name")
    if (confirm("Забронировать книгу \"" + name + "\" на 2 дня?")) {
        $.post("/api/zabronirovatKnigu.php", { name }, data => {
            data = JSON.parse(data)
            if (data.success) {
                alert("Книга \"" + name + "\" забронирована")
                location.reload()
            } else {
                alert(data.error)
            }
        })
    }
})

$(".cancel-book").on("click", function() {
    let name = $(this).data("name")
    if (confirm("Отменить бронь на книгу \"" + name + "\"?")) {
        $.post("/api/otmenitKnigu.php", { name }, data => {
            data = JSON.parse(data)
            if (data.success) {
                alert("Отменена бронь на книгу \"" + name + "\"")
                location.reload()
            } else {
                alert(data.error)
            }
        })
    }
})

$(".give-book").on("click", function() {
    let name = $(this).data("name")
    if (confirm("Выдать книгу \"" + name + "\"?")) {
        $.post("/api/vydatKnigu.php", { name }, data => {
            data = JSON.parse(data)
            if (data.success) {
                alert("Книга \"" + name + "\" выдана")
                location.reload()
            } else {
                alert(data.error)
            }
        })
    }
})

$(".get-book").on("click", function() {
    let name = $(this).data("name")
    if (confirm("Принять книгу \"" + name + "\"?")) {
        $.post("/api/prinyatKnigu.php", { name }, data => {
            data = JSON.parse(data)
            if (data.success) {
                alert("Книга \"" + name + "\" принята")
                location.reload()
            } else {
                alert(data.error)
            }
        })
    }
})
