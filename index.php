<?
    require "db.php";
    require "function.php";
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Библиотека</title>
        <link rel="stylesheet" href="/css/index.css">
    </head>
    <body>
        <header>
            <ul>
                <li><a href="/">Главная</a></li>
                <?
                    if (isset($_SESSION["polzovatel"])) {
                        if ($_SESSION["polzovatel"]->role == "admin") {
                            echo "<li><a href=\"/polzovateli.php\">Пользователи</a></li>";
                        } else {
                            echo "<li><a href=\"/knigi.php\">Книги</a></li>";
                        }
                    }
                ?>
                <li>
                    <?
                        if (isset($_SESSION["polzovatel"])) {
                            echo "<a href=\"/vyiti.php\">Выйти</a>";
                        } else {
                            echo "<a href=\"/voiti.php\">Войти</a>";
                        }
                    ?>
                </li>
            </ul>
        </header>
        <main>
            <?
                if (isset($_SESSION["polzovatel"])) {
                    echo "<b>Вы авторизованы как ".$_SESSION["polzovatel"]->login."</b>";
                    echo "<br>";
                    echo "<b>Ваша роль: ".role_full($_SESSION["polzovatel"]->role)."</b>";
                    echo "<br>";
                    echo "<b style=\"color: #136fb3\"><a href=\"/izmenit-parol.php\">Сменить пароль</a></b>";
                } else {
                    echo "<b>Вы не авторизованы</b>";
                }
            ?>
        </main>
    </body>
</html>
